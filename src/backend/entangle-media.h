/*
 *  Entangle: Tethered Camera Control & Capture
 *
 *  Copyright (C) 2009-2018 Daniel P. Berrange
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __ENTANGLE_MEDIA_H__
#define __ENTANGLE_MEDIA_H__

#include <sys/stat.h>
#include <sys/types.h>

#include <gexiv2/gexiv2.h>
#include <glib-object.h>

#include "entangle-control-group.h"

G_BEGIN_DECLS

#define ENTANGLE_TYPE_MEDIA (entangle_media_get_type())
G_DECLARE_DERIVABLE_TYPE(EntangleMedia,
                         entangle_media,
                         ENTANGLE,
                         MEDIA,
                         GObject)

struct _EntangleMediaClass
{
    GObjectClass parent;
};

const char *
entangle_media_get_filename(EntangleMedia *media);

time_t
entangle_media_get_last_modified(EntangleMedia *media);
off_t
entangle_media_get_file_size(EntangleMedia *media);

gboolean
entangle_media_delete(EntangleMedia *media, GError **error);

GExiv2Metadata *
entangle_media_get_metadata(EntangleMedia *media);
void
entangle_media_set_metadata(EntangleMedia *media, GExiv2Metadata *metadata);

G_END_DECLS

#endif /* __ENTANGLE_MEDIA_H__ */

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 *  tab-width: 8
 * End:
 */
