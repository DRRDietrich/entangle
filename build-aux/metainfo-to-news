#!/usr/bin/env python3

import os
import subprocess
import textwrap
import xml.etree.ElementTree as ET

WRAP_WIDTH = 61

source_dir = subprocess.check_output(
    ['/usr/bin/git', 'rev-parse', '--show-toplevel'],
    universal_newlines=True).strip()

tree = ET.parse(os.path.join(source_dir, 'src', 'org.entangle_photo.Manager.metainfo.xml.in'))
root = tree.getroot()

def print_underlined(string, with_character='-'):
    print(string)
    print(with_character * len(string))

def print_wrapped(text, prefix=''):
    wrapped = textwrap.wrap(text, width=WRAP_WIDTH - len(prefix))
    print(prefix + wrapped[0])
    for line in wrapped[1:]:
        print(' ' * len(prefix) + line)

def print_release_header(version, codename, date):
    print_underlined(f'Release {version} - "{codename}" - {date}')

def print_ul(ul):
    for li in ul.iter('li'):
        if not li.text:
            continue
        print_wrapped(li.text, prefix=' * ')

print('      Entangle News')
print('      =============')

for release in root.iter('release'):
    print()
    print_release_header(
        release.attrib.get('version'),
        release.attrib.get('codename'),
        release.attrib.get('date'),
    )
    print()
    description = release[0]
    for child in description:
        if child.tag == 'ul':
            print_ul(child)
            print()
        elif child.tag == 'p':
            print_wrapped(child.text)
            print()

print('-- End of news')
